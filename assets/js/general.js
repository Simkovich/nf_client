/**
 * Created by Alex.Simkovic on 19.11.2014.
 */
function viewport()
{
  var e = window, a = 'inner';
  if ( !( 'innerWidth' in window ) )
  {
    a = 'client';
    e = document.documentElement || document.body;
  }
  return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
}
function itemsCarousel(parentBlock, i) {
  /*get width of ul by multiplication length on li width*/
  var ulWidth1 = $(parentBlock).find(".mpage-block_ul li").outerWidth() * $(parentBlock).find(".mpage-block_ul li").length;
  /*set width of ul by multiplication length on li width*/
  $(parentBlock).find(".mpage-block_ul").css("width",ulWidth1+"px");

  $(document).on("click",parentBlock+" .arr-right",function(ev){
    ev.preventDefault();
    var w = $(this).closest(parentBlock).find(".mpage-block_ul li").outerWidth();
    /*get value of attribute style right*/
    var getStyleRight = $(parentBlock).find(".mpage-block_ul").css("right");
    if ( parseInt(getStyleRight) == parseInt(ulWidth1-(w*i))) {
      $(this).closest(".mpage-block_t").next().find(".mpage-block_ul").animate({
        right: "0"
      },400);
    }
    else {
      $(this).closest(".mpage-block_t").next().find(".mpage-block_ul").animate({
        right: "+="+w
      },400);
    }
  });

  $(document).on("click",parentBlock+" .arr-left",function(ev){
    ev.preventDefault();
    var w = $(this).closest(parentBlock).find(".mpage-block_ul li").outerWidth();
    /*get value of attribute style right*/
    var getStyleRight = $(parentBlock).find(".mpage-block_ul").css("right");
    if ( parseInt(getStyleRight) == 0) {
      return false
    }
    else {
      $(this).closest(".mpage-block_t").next().find(".mpage-block_ul").animate({
        right: "-="+w
      },400);
    }
  });
}

$(function(){
  var isIE = /*@cc_on!@*/false || !!document.documentMode;   // At least IE6
  $(document).on("click",".header-search i",function(){
    $(this).parent().find(".header-search-input").fadeIn();
  });

  $(".header-search-input").focusout(function(){
    $(this).fadeOut();
  });

  $(document).on("click",".header-tablet-search i",function(){
    $(this).parent().find(".header-tablet-search-input").fadeIn();
  });

  $(".header-tablet-search-input").focusout(function(){
    $(this).fadeOut();
  });

  $(document).on("click",".header-mobile-search i",function(){
    $(this).parent().find(".header-mobile-search-input").fadeIn();
  });

  $(".header-mobile-search-input").focusout(function(){
    $(this).fadeOut();
  });

  if (viewport().width > 991) {
    itemsCarousel(".__popular-ideas", 3);
    itemsCarousel(".__our-projects", 4);
    itemsCarousel(".__blogs", 3);
  }

  if (viewport().width > 767 && viewport().width < 991) {
    itemsCarousel(".__popular-ideas", 2);
    itemsCarousel(".__our-projects", 3);
    itemsCarousel(".__blogs", 2);
  }
  if (viewport().width < 768) {
    itemsCarousel(".__popular-ideas", 1);
    itemsCarousel(".__our-projects", 1);
    itemsCarousel(".__blogs", 1);
    $(".mpage-block_li").css("width", (viewport().width - 17) + "px");
    if (isIE) {
      $(".mpage-block_li").css("width", (viewport().width) + "px");
    }
  }

  /*modals manipulations*/
  $(document).on("click","#restore",function(){
    $("#authModal").modal("hide");
    $("#restoreModal").modal("show");
  });
  $(document).on("click","#reg",function(){
    $("#restoreModal").modal("hide");
    $("#authModal").modal("hide");
    $("#regModal").modal("show");
  });

  $(document).on("click","#auth",function(){
    $("#regModal").modal("hide");
    $("#restoreModal").modal("hide");
    $("#authModal").modal("show");
  });
});

$(window).resize(function(){
  var isIE = /*@cc_on!@*/false || !!document.documentMode;   // At least IE6
  if (viewport().width > 991) {
    itemsCarousel(".__popular-ideas", 3);
    itemsCarousel(".__our-projects", 4);
    itemsCarousel(".__blogs", 3);
  }

  if (viewport().width > 767) {
    itemsCarousel(".__popular-ideas", 2);
    itemsCarousel(".__our-projects", 3);
    itemsCarousel(".__blogs", 2);
  }
  if (viewport().width < 768) {
    itemsCarousel(".__popular-ideas", 1);
    itemsCarousel(".__our-projects", 1);
    itemsCarousel(".__blogs", 1);

    $(".mpage-block_li").css("width", (viewport().width-30) + "px");
    if (isIE) {
      $(".mpage-block_li").css("width", (viewport().width) + "px");
    }
  }
});
